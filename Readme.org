* William Shakespeare on Time and Life

  A manager-friendly version of William Shakespeare's wisdom.

  This is in the public domain.  Enjoy.
